CC = cc
CFLAGS = -O3 -Wall -Wextra
LDFLAGS =
LIBS = -lncurses

all: tetris

tetris: tetris.o
		$(CC) $(LDFLAGS) $(LIBS) $^ -o $@

.c.o:
		$(CC) $(CFLAGS) -c $< -o $@

clean:
		@clear
			@rm -f *.o tetris valgrind.log *~
check:
		valgrind --leak-check=full \
			 --log-file="valgrind.log" \
			 ./tetris
		@cat valgrind.log | grep "ERROR SUMMARY"

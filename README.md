# Tetris
#### A very simple implementation of Tetris in C using ncurses.

### Usage
```sh
make
./tetris
```

### Game
```
 |............|
 |............|
 |............|
 |............|
 |............|
 |............|
 |............|
 |............|
 |............|
 |............|
 |........#...|
 |........#...|
 |........#...|
 |........#...|
 |#...........|
 |#..#..#....#|
 |########.###|
 |########.###|
 |########.###|
 |########.###|
 +------------+

 Score: 0
```
### Control
* <kbd>arrows</kbd> - move, rotate, lower down slowly;
* <kbd>space</kbd> - quickly lower;
* <kbd>esc</kbd> - exit.

### License: ISC

